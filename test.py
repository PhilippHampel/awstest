#in diesem Abschnitt beginnen wir mit dem Einladen der Libraries
import tensorflow as tf 
import numpy as np

#in diesem Abschnitt laden wir die Daten (Bilder und Labels) aus der Datenbank "Mnist Fashion" ein
mnist = tf.keras.datasets.fashion_mnist
(training_images, training_labels), (test_images, test_labels) = mnist.load_data()

#in diesem Abschnitt formen wir unsere Daten so um, dass unser neuronales Netz mit den Daten arbeiten kann
training_images = training_images.reshape(60000, 28, 28, 1)
training_images = training_images / 255.0
test_images = test_images.reshape(10000, 28, 28, 1)
test_images = test_images / 255.0

#in diesem Abschnitt "bauen" wir uns neuronales Netz
model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])

#in diesem Abschnitt trainieren wir unser neuronales Netz
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(training_images,training_labels, epochs=4)

#in diesem Abschnitt testen wir unser neuronales Netz mit den Testbildern des Datensets, welche unser neuronales Netz noch nie gesehen hat
classes = model.predict(test_images)
predicted_classes = np.argmax(classes, axis=1)
print(classes[0])
print(test_labels[0])

#in diesem Abschnitt schauen wir uns das Bild an, welches wir vorher zum Testen genutzt haben

mnist = tf.keras.datasets.fashion_mnist

(training_images, training_labels), (test_images, test_labels) = mnist.load_data()

import matplotlib.pyplot as plt

plt.imshow(test_images[0], cmap='Greys_r')

#in diesem Abschnitt laden wir die Libraries ein
import numpy as np
from google.colab import files
from keras.preprocessing import image
import cv2
import matplotlib.pyplot as plt

uploaded = files.upload()

for fn in uploaded.keys():
  #in diesem Abschnitt laden wir die Bilder rein; wir nutzen eine for Schleife, um mehrere Daten gleichzeitig einladen zu können 
  path = '/content/' + fn
  img = cv2.imread(path) 
  
  #in diesem Abschnitt formartieren wir unsere Bilder, sodass sie zu unserem neuronalen Netz passen
  img = cv2.resize(img,(28,28))
  img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  x = image.img_to_array(img, dtype=np.float32)
  print("top left pixel value:", x[0,0])
  if x[0,0] > 250:
    # white background
    print("needs to be inverted!")
    x -= 255
    x *= -1
  x = x / 255.0
  # x = np.expand_dims(x, axis=0)  # das brauchst du nicht weil du danach ja reshapest
  x = x.reshape(1, 28, 28, 1)
  plt.imshow(img, cmap='Greys_r')
  plt.show()  
  
  #in diesem Abschnitt lassen wir unser neuronales Netz einschätzen, um welches Kleidungsstück es sich handelt; Bedenkt, dass Python ab 0 zählt, somit müsst ihr bei der ausgegebenen Zahl +1 rechnen
  classes = model.predict(x)
  plt.bar(range(10), classes[0])
  plt.show()
  print("prediction: class", np.argmax(classes[0]))
  
